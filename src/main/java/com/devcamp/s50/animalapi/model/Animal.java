package com.devcamp.s50.animalapi.model;

public abstract class Animal {
    private String name ;



    public Animal(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return "Animal [name=" + name + "]";
    }



    public String getName() {
        return name;
    }
    
}
