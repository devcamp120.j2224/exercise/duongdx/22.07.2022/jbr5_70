package com.devcamp.s50.animalapi.model;

public class Cat extends Mammal{

    public Cat(String name) {
        super(name);
        //TODO Auto-generated constructor stub
    }
    public void greets(){
        System.out.println("MeoMeo");
    }
    @Override
    public String toString() {
        return "Cat [Mammal [Animal [name=" + super.getName() + "]]]";
    }
    
}
