package com.devcamp.s50.animalapi.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s50.animalapi.model.Animal;
import com.devcamp.s50.animalapi.model.Cat;
import com.devcamp.s50.animalapi.model.Dog;

@Service
public class AnimalService {
    Dog Dog1 = new Dog("Cho De");
    Dog Dog2 = new Dog("Cho chet");
    Dog Dog3 = new Dog("Cho ma");
    Cat Cat1 = new Cat("Meo Mu");
    Cat Cat2 = new Cat("Meo ma");
    Cat Cat3= new Cat("Meo hoang");

    public ArrayList<Animal> getAnimalAll(){
        ArrayList<Animal> AllAnimal = new ArrayList<>();

        AllAnimal.add(Dog1);
        AllAnimal.add(Dog2);
        AllAnimal.add(Dog3);
        AllAnimal.add(Cat1);
        AllAnimal.add(Cat2);
        AllAnimal.add(Cat3);

        return AllAnimal ;
    }

}
