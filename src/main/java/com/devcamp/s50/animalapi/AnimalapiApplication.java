package com.devcamp.s50.animalapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnimalapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnimalapiApplication.class, args);
	}

}
