package com.devcamp.s50.animalapi.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s50.animalapi.Service.AnimalService;
import com.devcamp.s50.animalapi.model.*;


@RequestMapping("/")
@RestController
@CrossOrigin
public class AnimalController {
    @Autowired
    private AnimalService animalService;

    @GetMapping("/cats")
    public ArrayList<Animal> getDanhSAchMeoAPi(){
        ArrayList<Animal> DanhSach = animalService.getAnimalAll();
        ArrayList<Animal> DanhSachMeo = new ArrayList<>();
        for (Animal animal : DanhSach) {
            if(animal instanceof Cat){
                DanhSachMeo.add(animal);
            }
        }
        return DanhSachMeo ;
    }

    @GetMapping("/dogs")
    public ArrayList<Animal> getDanhSAchChoAPi(){
        ArrayList<Animal> DanhSach = animalService.getAnimalAll();
        ArrayList<Animal> DanhSachCho = new ArrayList<>();
        for (Animal animal : DanhSach) {
            if(animal instanceof Dog){
                DanhSachCho.add(animal);
            }
        }
        return DanhSachCho ;
    }
}
